---
idf:
  net_config: &net_config
    admin:
      interface: 0
      network: 10.253.0.0
      mask: 24
      gateway: 10.253.0.1
      dns: 10.253.0.1

os_infra:
  net_config: *net_config
  user:
    name: k8s_rancher-ci
    is_admin: false
  tenant:
    name: k8s_rancher
    public_net_name: ext-net
    flavor_prefix: k8s_rancher
    quotas:
      cores: 272
      ram: 458752
      gigabytes: 4400
      floating_ips: 20
      instances: 20
      snapshots: 20
      volumes: 1000
      server_group_members: 20
  images_to_push:
  image_default: debian-10
  image2user_mapping:
    debian-10: debian
  nodes_roles:
    control01: [kube-master, monitoring, nfs-server, etcd]
    control02: [kube-master, monitoring, etcd]
    control03: [kube-master, monitoring, etcd]
    compute01: [kube-node]
    compute02: [kube-node]
    compute03: [kube-node]
    compute04: [kube-node]
    compute05: [kube-node]
    compute06: [kube-node]
  roles_group:
    k8s-cluster:
      - kube-master
      - kube-node
    k8s-full-cluster:
      - k8s-cluster
      - jumphost
      - monitoring
      - etcd

  dns:
    update: true
    provider: gandiv5
    zone: onap.eu
    name: k8s9cinder
    master: control01

  onap:
    global_storage:
      enabled: true
      class: hdd
      fast_class: ssd-fast
      rwx_class: nfs

  kubernetes:
    storage_classes:
      - name: hdd
        parameters:
          availability: nova
          type: public
        provisioner: cinder.csi.openstack.org
      - name: ssd
        parameters:
          availability: nova
          type: ssd
        provisioner: cinder.csi.openstack.org
      - name: ssd-fast
        parameters:
          availability: nova
          type: ssd-fast
        provisioner: cinder.csi.openstack.org
    certmanager:
      webhooks:
        - name: letsencrypt-gandi
          git: https://github.com/sylvainOL/cert-manager-webhook-gandi.git
          namespaces:
            - istio-system
            - keycloak
          certificates:
            - name: keycloak
              namespace: keycloak
              secretName: keycloak-tls
              dnsNames:
                - "{{ keycloak_fqdn | default('keycloak.k8s3.onap.eu') }}"
    helm:
      repositories:
        - name: jetstack
          url: https://charts.jetstack.io
        - name: codecentric
          url: https://codecentric.github.io/helm-charts
        - name: bitnami
          url: https://charts.bitnami.com/bitnami
        - name: minio
          url: https://helm.min.io/
    charts:
      cert-manager:
        chart: jetstack/cert-manager
        namespace: cert-manager
        istioEnabled: false
        content: |
          ---
          installCRDs: true
          prometheus:
            servicemonitor:
              enabled: true
      keycloak:
        chart: codecentric/keycloak
        namespace: keycloak
        istioEnabled: true
        content: |
          ---
          keycloak:
            serviceAccount:
              create: true
            ingress:
              enabled: true
              path: /
              servicePort: http
              hosts:
                - "{{ keycloak_fqdn | default('keycloak.k8s3.onap.eu') }}"
              tls:
                - hosts:
                    - "{{ keycloak_fqdn | default('keycloak.k8s3.onap.eu') }}"
                  secretName: keycloak-tls
            persistence:
              deployPostgres: true
              dbVendor: postgres
          postgresql:
            serviceAccount:
              enabled: true
            persistence:
              enabled: true
              storageClass: ssd
              size: 2Gi
            metrics:
              enabled: true
              serviceMonitor:
                enabled: true
              securityContext:
                enabled: true
          prometheus:
            operator:
              enabled: true
      minio:
        chart: minio/minio
        namespace: minio
        istioEnabled: true
        content: |
          ---
          accessKey: "{{ lookup('env','S3_ACCESS_KEY') }}"
          secretKey: "{{ lookup('env','S3_SECRET_KEY') }}"
          replicas: 1
          persistence:
            size: 100Gi
            storageClass: hdd
          service:
            type: LoadBalancer
            nodePort: 30345
          metrics:
            serviceMonitor:
              enabled: true
          updatePrometheusJob:
            podAnnotations:
              sidecar.istio.io/inject: "false"
      nfs-server-provisioner:
        chart: stable/nfs-server-provisioner
        namespace: nfs-server
        enabled: true
        storageClass: ssd
      postgres:
        chart: bitnami/postgresql
        namespace: helm
        content: |
          ---
          global:
            imageRegistry: docker.nexus.azure.onap.eu
            postgresql:
              postgresqlDatabase: helm
              postgresqlUsername: helm
              postgresqlPassword: "{{ lookup('password', '/tmp/passwordUserPG chars=ascii_letters,digits') }}"
              replicationPassword: "{{ lookup('password', '/tmp/passwordRepPG chars=ascii_letters,digits') }}"
          replication:
            enabled: true
            readReplicas: 2
            synchronousCommit: "on"
            numSynchronousReplicas: 1
          metrics:
            enabled: true
          persistence:
            size: 1Gi
          service:
            type: NodePort
            nodePort: 30347
          readReplicas:
            service:
              nodePort: 30346

  openstack:
    cinder: true
    lbaas: true
    octavia: true

  ssh_pub_key_default: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDB0B69s5gYL0SCa7X\
    DxzonxhWz0jvgxaG+lCg1W9YJ4+DcANRJkZJ27RSdx/39pQkHyvV4NhGu52Q7M/NtjTVZ3kX+DF\
    LXKrwau/+lSw78grMKReG8/dpdhWE7E33k6NSXfHyjtr7Qo9adxWDZ0NP6Sc9wEGMt5QJ/KValY\
    dUisCgu43IitJS1BPkJFLApKZmix70E3q5kNvd6AYYD07zkKN9OBty3D8IIv/X/ehw3VpbhibL1\
    ay+FfVSGdQeWdJV9OgN8gCYPZfMgY8OVHNcvnnzXRqJtY13VUmdXAd1bb8BRp54JpGZs/jWJiUR\
    PFnb0oUKyssgNorKOq8qyri9590ivou44UY/TFeqj/pzPXNAFNotC/CLzbyJjwMeZY52a9UtcsI\
    vNLYcf125dJurzT8myAiU97vqKKiTpWj2LUbqmA3kTkVnRX5DIE4tb6FWUf1LK6rNrUy2aIyGlt\
    ZcKbkLM+2Jnjr5GYGEYVb5ZTSpInfEJ8sgBLjwpVHzcs4NGYE4U9kVx0JMOp0B79zrJefZDNxX/\
    V1asVolJdMyTnctBiTSgT9d+rNd5cdj1eycBN0CVRfkNjWpkLeGSHCsJD5wpsgCPo1qH6y0EExh\
    gVnfOvluK0/13XzIxAbx2G3UvdMYq4I/6cbSN31OefyPwaus87k9z4NTefsW6jYO1Fw== user@\
    rebond.opnfv.fr

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDrzN1AvD7vSGbZLN7dupFHELOhSzgeGOS13ZV\
    leyz8L2fjzX2qANA/gusWCrvn2WON8VcI7vpi8G1ChXMOtHhLtG9TdFl3+pwrSKmUAENxH9/8oD\
    kicb6ZfLRTvXvQfL6OrWcLQDdcfp6XVaMoiyMKZ0P5JcQg7L8Rg6TN21I1BwMmeYG1xpgcjtq20\
    FYCjuiGINvvF+hm7kpaCJXHteYKddklwBEnC9MWcTKRvT1GnnDCAVXUqOdBALU7cP4As2FMtJq9\
    a846Ru9ZHulnflaK6KR713uT5nfTjRyiNB6KcpwNsadJLZN1WR+dG8kfcMwI1/UFDJ4+y8TCbVw\
    3rbuX david.blaisonneau@gmail.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCpk6Inc6RbFmiAVOYSUr0VWPrtubrpt1IqLix\
    htuwSJEJUgg7Qz1cCkIW3ntJX56aXh8voBLnGHSKsuDpCCBMdKPFETCQ3TXPvsnRnzoJHnO43MG\
    xgK2UNeBIt5yrAsB7HAP7DGT2NRelzSwPg1jchzb/01j7wjuKUcGLH8nwfo43A9pAxod68nAAy0\
    MtQqWnPdkMJCe+sxmws3OheQmQjP++9p3c98yHvxeG4Tdoq2V1wz7Krhf5zn2d9WPDaFD6sW/jW\
    m8+NqBuzCDLOHduVtD54D3oOKcnxX3lHIFa+jhCxbRVDqtqonQDQPUJTKbSNomuizsQtMBOe3T2\
    MXtVp david.blaisonneau@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDB22+oh+TaQs5XbJtDY3elDWHnTlPCOwqr0kh\
    wYGn/teOr9hPDtqADPjaymoowNDbFIB9CZleZuLFAIryCplb563EwHoaGllRaBom/Alzj3QRIhx\
    FjhWCBzes+ix6NRqKQZ7wPGB7u5QuQCvqlMgLKDNz2UmPoIoYLrhrFyUE5cxWtZ2vmSMTkOpLwO\
    Aw+9UWMww6jzCLNcQjl25zyfSLXVCLLbuyBUza0SvcyxH4glbjTx3BjdCp6ovDDhO2z9tvofvzF\
    j5ceGlohD40ETiROT9C7hKj7Bn2MPDCKPEEjBVjkh9YkpUD9W6uW/t1loGoajLZeJEbN946L3Lt\
    Yi0wz morgan.richomme@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDmQpGN/RX+qEGmBEzg2RKWBTgKz3WCmv1pPeQ\
    3pAWzK1wdYeAr7JMD2YaId+ckj+k8hn6YspYT1mR9SBeSsapn1a09Nosb0Duta1XS6RaytMKJm1\
    tFlLjv5QUwrIN4P0feNBnafZsLKvPAY/x/E9lOtCu/qCVSkzvlOCxDpVS5ZR2NDlOG+8k3QgflU\
    CwrkRfkVitFVuaAmNwHm9s3gsoXH1dxRFAddBPaHtb96ShUBZeguVg6q6bbwFgBC9tD8fggbKDJ\
    ekSTFn20abH5HRdh/VUWl6X0YwO/JhHkrqh+GLA51IiOBAB02xlwi8Abp8/wYL8x+eH0fThAi7V\
    slRav nicolas.edel@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqbo9WQwjZp6Op6awsgRaBdR+gJhbmMHOm+2O\
    l7TJlXe2wV5r2vFQhbmd0GHTdz4g8MH+y7oO5637RoAMZlY4ieZdSVGZZ5HvIoMqoS7FVT6u1kv\
    VTVBmlpEgF4gXLsJi3s4bGlXqVnYA42SoKX65DhvHgLIScL1E6uS63MLRP+clFchNASyhz8CgRP\
    dazE7vy4LJJNE8C6hj0MVSOVasJKMZQAmqjSFkuMy0ECNDj5sQmoPYozpvM9xsP/UWiW8MCWuTh\
    33JRBPH/06OIUMVN0dD+xhfVgYY+aueFRjs4oaMm2kfgiTJsQwuAziLrvnusSse+idq9t49YeiA\
    PGpuZ sylvain.desbureaux@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDy4J5MFwpEp0ucKffPyDKGceCynvUqjlsO3r\
    dJRHNA4MDOXygYdMaqhK8FYB9ti2yqAS5AMOR6niio9s06a7mxOGVDaUvlT8L6UUrZAhFYDhWUe\
    amMWAdlG8DWhzfQkYAchbItgxaaSRa8gqZdoyB6hq+JY17K8OKnItr54twhHhsxvAV5lT5C50mG\
    V7aacrWQme/prwDNZsW0aZMlnRSq6KgFidvAL8YaWbSc4yqZvT57SmNRxeOh/MVKhj+F+kKWWDI\
    8nOX/dDFP+QhZeJx0lXHTYNPgzl2S2IAdrbZBcHUpwcnZofToSqlOBsJ4VOiohRngw+abozAGO3\
    9UAvv+3WkT7e0opRRO+OKQVdzkKszNEnTPqKYl9fMTr4HgAV1CAa7PlWYq5IIDMERfPuc0zP5kU\
    qS2SoUyclUI2gDcJpZ6UCt5MAFIbWNlE8KyaEGtMrM4t8z3Zvn8/IdWAREA0RVHpJVgNtKPvvLI\
    NB8O1J4UDBN4fV5vVN+JIHrpDrxz7hKN5pLbl6j0T4SVqQneb0Whbz63P4WF22ddY+bCP7+C9p3\
    pmDibAQkh0aKTyK3J3snvI2mtZ9QfDlN5mL4Rao288SbsVC9pkYUStowmrGCyUf6Txk7F2pP66+\
    pbx8rTON99Hqcv3r8QbKY/k8ORqIT5SCq3fex4p4hRLBk7Q== veronica.quintunarodrigue\
    z@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBlukmibuf0LMrK5NmZkxIziYrOovBzYG8JmQ\
    KONN3KtRZBy9ooL5JBFfmdWMvdcWoLee23R024DwWGF1ooR9SyolXQ+K7lbsSzw0NR/+feRs3of\
    asxj0bXJsWjC48C7eD8v48Y2PMs5uP5Yp/RMOThHVN7w2gbm2OZGG5MIV1ixML/coqo39clm8dC\
    pFhSZmPfmVFliOBW8dFk+DSoKClVy7FH56qEzKoenLJy/+j0B10/5t6T7Sk1QUWDOfIikBomGa2\
    gZAozgGFnuoK9b7FBwlyZiYbbbL4EShiqkEaY938zYcXr1/bqcZWj/F30hK5ddo6f1IDwr57U6l\
    1W5Vd thierry.hardy@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCaK+5td8aCRvDYRET/9jFBF5tCUG/jwfogC1q\
    PXS6f6ou+jLDiNa2e1l/2SbsiU5GdQzT2dfFe50HHiwn15KX5D/0QDs4W8/TAaSXMFWw2ioe1n1\
    A4kXsN2MwPO6HWWhcpWhj+FCGQvU7dP1Divqb4HOpEO2AIkRMH+pH+v/22EsNHBd8xY/JpoC//n\
    0/kzdQ9Fp5pdkoz7VdQaVowu2UsJBrNKVjhNQd31Lw9H+EGNwxCyV4YTTXQRUQkZmnx6kNrtIMQ\
    n4xEw9eFMjHvjjOxO9c2qt++LB0eNfgAgDMHSDkTUfSuF3Tr1YMFy0h+M/nqXgTNme/HAJbdDY+\
    eFRCL natacha.chereau@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAgq1uPzKI/Jeu4YVc2ZASGoZrHV2sr8n+DmeSBIk\
    RhlBgF74Nb3hG6rHObmSYg6uW5iiYqATgKv6AJThVd1QRk+7LgZOxQu/hqv24yW0Su2ec1AqHBZ\
    e74xmx3vxUydpZbcUofRv0QlwNeyymEYyuAk3frLsqjUIQAHqzCO6po21DCk9mTm5h9WcpDS+5B\
    67GXfvNq5DnCz5f2eDHTyNR02SdQKmjfH26Hx558IWl3I1AqHME7uOxRYFvMavst4CMAQ7++WzF\
    fJhXsJBZAgVabgvOlEaJsWS8Ojgscukf3MPqu49QGQ/A4/qrePdcaROnh95vmUw6Yx0tE8L0f44\
    FXw== fabian.rouzaut@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5mk1mmSr5HnF6KQFhHZ4KlHSEhg8o/CSkz5p\
    iNI4a25EeOcIDlDHGjYUp8vw+fB3Ru8F6Z03AdpQQqZYtKvU1hXNyyzvk8JEA7gV+ZtJlOebynq\
    2LPku4izk7bPaMpXLGuyVywhEH+RjtAuvuiigiUUHt+/OQFbP+a6Xcn2ay/Ux8/MIMTEaZLExdv\
    ZtHMmwYkahyavu2oUN6k9Z2bi7CM7htmXmxPmhydpDRjDOXfg7L79SJwRSuNu+SIjVc0tzOSX3i\
    MiIbRBkOx1ddVr7lcYL6P7c4VjBleYgtTKhrX0RqUUBo97VqskUs/zBfuAkQHNn6Zti7Q/7lF5m\
    mqU/p virginie.lefilleul@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRslHbwKe4AzMa9dRXnpEDjrbqMvPuGxG2zzZ\
    KbKWsqtzUhgIAMhUL6vw4ePI34bN6uqq8qsQDDu5JYJ3sO1WwpFhX0ZEvWohQK55oe94kfXfnbu\
    sOwRfd8uiWh2s8lfCL7BlbruKGIJcueX+bMLJfUPo3vo5Ae3bBn+J4sG1tLLpf9UxvuXmnZiRyv\
    QJjCy2f6BOeS2AzRhezkC23IS2gylzG3I3MT1acw3i9wAF2yZ2ca++6MdLb6e6Bu30Y9rO7PAj8\
    76myts4GaBhbXmi1+rjb0HbUm3V9wxG7drKv7C0y0nftBS0Q9yCKqld96WQgseYL3CcwEe4qVqn\
    a7DxWQCJROJRi9/DcSSoL73jPcfSWyRqeutWp55S/ajvseAG2yZExbMiGOmW5qsyAGMigjGik59\
    H7gSJR0RitQtGpoqBzsuFaGgYnpfUkAgnjdqqlLT7iRO6OZAXVVSZODbb6LE+2av4B0Yp7SHx8U\
    Rps4CGBS8HyGHQNelnOmra8SnVmXtOhYU87kdOoPaTU7GkuYiy8ctewSoHdgX40tRbB45AMlc+L\
    avf+4hx/5K/u50gei19tUM9aKf6TmdH8LFOPtwUtMd/4nNVYmuwdEFqTVTkVPYdpIF75N5NDF7d\
    uDRyYakTPG/rvj9l0zT5V7sfc3QA0TVNdHu6NLPHHqvkjsQ== k.opasiak@samsung.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC6vy50hv15I2QcP0YMEUAD1yh56OcuaLenpjC\
    JQVDHSP4sg/dDXldewDu0YOzpBfMTSKFjciCx8l3pnP80LTFwHLvdpW5fqjPzLZOm9NrfPgRcE+\
    Q09wrSelPueYZlIWf831XBEafrFUVgj8L8pz7kJeQPGFVh0/wykeUIsj3QPBQcQA1xJi0+Hm7+W\
    6Cm7kyrhJEO86mG3buU1BL4OTAbFrA2CSAy3LkfpCX/7ySLLuw4T8yNRltV0dAatWBny+X4pKhh\
    fQot5exivAIs7S9z66r2yUq4Qw1BmkHIqKIwOqykuUdpwJPCbOt7d+kmH8j8k/w2MCfb2mNWxK7\
    G39cW1fKhTouwakNklgA2BeVZ10MMyRKMna+ReXi6BQqkPKzfeXu9U4/wk5UPxuHZDOZTcSEoUH\
    uL9Z6pYKmN6pgrMdXmF05zGJu41aT5hqoYzCHQ/xD7TBGtDxQFLyPLcdKIXVXl0BVSY7m6wzi8D\
    632qyMvXUpPgDRRIZWmQvnVNK0= otto@ITEM-S097205

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+FaunOlXUtpo/FsFE9CNXWJeCiNgqojLa2hr\
    XSzo4NhBBHVycyovTcv1Q982J9EBGAGkM8Mmo8N5kKTrReQIn2OleLQ9/X/C17z5yBmIkahpYxb\
    NvhGUKDDdWtiTjS2d4ngoVSflVJvY1MQB/wPfFBzcclJFRqydBwcXYtee03KY+Hfg3YUH2epIxe\
    GjBGXTxxPTDTFp1L8ScSi23q/kivP5tJX1PBlbk2h14y3idGJusYvh4BAVc3y+M4wphMiw5oR7l\
    EZM5UodBRxEJ+qqlHmEBalSt5VLqqzqnER/8NBLfuAbICytB26CgzH5nxMfOmo6T7KNVdX4MYqg\
    Lw9l8pqVeL59TPjh0FeaL2vIV1rqJNC3o57rXEpONjkLlLt1XKtHNBhlR4m2IB/V0rqK+kzeUfW\
    3+8HcduYuwCtHBBYlEq1TiIVXX9RIKEFPtMQ2KH2yyWMZ45VjvtnrXcZzMlkcYr7RGyQlse3IC9\
    nsukrBahWpzAplBAk1g7c= jack.lucas"
