---
idf:
  net_config: &net_config
    admin:
      interface: 0
      network: 10.253.0.0
      mask: 24
      gateway: 10.253.0.1
      dns: 10.253.0.1

os_infra:
  net_config: *net_config
  user:
    name: k8s_rancher-ci
    is_admin: false
  tenant:
    name: k8s_rancher
    public_net_name: ext-net3
    flavor_prefix: k8s_rancher
    quotas:
      cores: 272
      ram: 458752
      gigabytes: 4400
      floating_ips: 20
      instances: 20
      snapshots: 20
      volumes: 1000
      server_group_members: 20
  images_to_push:
  image_default: debian-9
  image2user_mapping:
    debian-9: debian
  nodes_roles:
    control01: [kube-master, monitoring, etcd, nfs-server]
    control02: [kube-master, monitoring, etcd]
    control03: [kube-master, monitoring, etcd]
    compute01: [kube-node]
    compute02: [kube-node]
    compute03: [kube-node]
    compute04: [kube-node]
    compute05: [kube-node]
    compute06: [kube-node]
  roles_group:
    k8s-cluster:
      - kube-master
      - kube-node
    k8s-full-cluster:
      - k8s-cluster
      - jumphost
      - monitoring
      - etcd

  dns:
    update: true
    provider: gandiv5
    zone: onap.eu
    name: k8s18cinder
    master: control01

  onap:
    global_storage:
      enabled: true
      class: hdd
      fast_class: ssd-fast
      rwx_class: nfs

  kubernetes:
    storage_classes:
      - name: hdd
        parameters:
          availability: nova
          type: public
        provisioner: cinder.csi.openstack.org
      - name: ssd
        parameters:
          availability: nova
          type: ssd
        provisioner: cinder.csi.openstack.org
      - name: ssd-fast
        parameters:
          availability: nova
          type: ssd-fast
        provisioner: cinder.csi.openstack.org
    helm:
      repositories:
        - name: bitnami
          url: https://charts.bitnami.com/bitnami
        - name: longhorn
          url: https://charts.longhorn.io
        - name: jetstack
          url: https://charts.jetstack.io
        - name: minio
          url: https://helm.min.io/
    charts:
      longhorn:
        chart: longhorn/longhorn
        namespace: longhorn-system
        istioEnabled: true
        content: |
          ---
          persistence:
            defaultClassReplicaCount: 1
          defaultSettings:
            createDefaultDiskLabeledNodes: true
            defaultDataPath: /var/lib/longhorn/
            replicaSoftAntiAffinity: false
            storageOverProvisioningPercentage: 600
            storageMinimalAvailablePercentage: 15
            upgradeChecker: false
            defaultReplicaCount: 1
            defaultDataLocality: disabled
            guaranteedEngineCPU: 0.25
            defaultLonghornStaticStorageClass: longhorn-static
            backupstorePollInterval: 500
            taintToleration: key1=value1:NoSchedule; key2:NoExecute
            registrySecret: registry-secret
            autoSalvage: false
            disableSchedulingOnCordonedNode: false
            replicaZoneSoftAntiAffinity: false
            volumeAttachmentRecoveryPolicy: never
            nodeDownPodDeletionPolicy: do-nothing
            mkfsExt4Parameters: -O ^64bit,^metadata_csum

      cert-manager:
        chart: jetstack/cert-manager
        namespace: cert-manager
        istioEnabled: true
        content: |
          ---
          installCRDs: true
          prometheus:
            servicemonitor:
              enabled: true
      minio:
        chart: minio/minio
        namespace: minio
        content: |
          ---
          accessKey: "{{ lookup('env','S3_ACCESS_KEY') }}"
          secretKey: "{{ lookup('env','S3_SECRET_KEY') }}"
          replicas: 1
          persistence:
            size: 100Gi
            storageClass: hdd
          service:
            type: NodePort
            nodePort: 30345
          metrics:
            serviceMonitor:
              enabled: true
      nfs-server-provisioner:
        chart: stable/nfs-server-provisioner
        namespace: nfs-server
        enabled: true
        storageClass: ssd
      postgres:
        chart: bitnami/postgresql
        namespace: helm
        content: |
          ---
          global:
            storageClass: ssd-fast
            imageRegistry: docker.nexus.azure.onap.eu
            postgresql:
              postgresqlDatabase: helm
              postgresqlUsername: helm
              postgresqlPassword: "{{ lookup('password', '/tmp/passwordUserPG chars=ascii_letters,digits') }}"
              replicationPassword: "{{ lookup('password', '/tmp/passwordRepPG chars=ascii_letters,digits') }}"
          replication:
            enabled: true
            readReplicas: 2
            synchronousCommit: "on"
            numSynchronousReplicas: 1
          metrics:
            enabled: true
          persistence:
            size: 1Gi
          service:
            type: NodePort
            nodePort: 30347
          readReplicas:
            service:
              nodePort: 30346

  openstack:
    cinder: true
    lbaas: true
    octavia: true

  ssh_pub_key_default: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDB0B69s5gYL0SCa7X\
    DxzonxhWz0jvgxaG+lCg1W9YJ4+DcANRJkZJ27RSdx/39pQkHyvV4NhGu52Q7M/NtjTVZ3kX+DF\
    LXKrwau/+lSw78grMKReG8/dpdhWE7E33k6NSXfHyjtr7Qo9adxWDZ0NP6Sc9wEGMt5QJ/KValY\
    dUisCgu43IitJS1BPkJFLApKZmix70E3q5kNvd6AYYD07zkKN9OBty3D8IIv/X/ehw3VpbhibL1\
    ay+FfVSGdQeWdJV9OgN8gCYPZfMgY8OVHNcvnnzXRqJtY13VUmdXAd1bb8BRp54JpGZs/jWJiUR\
    PFnb0oUKyssgNorKOq8qyri9590ivou44UY/TFeqj/pzPXNAFNotC/CLzbyJjwMeZY52a9UtcsI\
    vNLYcf125dJurzT8myAiU97vqKKiTpWj2LUbqmA3kTkVnRX5DIE4tb6FWUf1LK6rNrUy2aIyGlt\
    ZcKbkLM+2Jnjr5GYGEYVb5ZTSpInfEJ8sgBLjwpVHzcs4NGYE4U9kVx0JMOp0B79zrJefZDNxX/\
    V1asVolJdMyTnctBiTSgT9d+rNd5cdj1eycBN0CVRfkNjWpkLeGSHCsJD5wpsgCPo1qH6y0EExh\
    gVnfOvluK0/13XzIxAbx2G3UvdMYq4I/6cbSN31OefyPwaus87k9z4NTefsW6jYO1Fw== user@\
    rebond.opnfv.fr

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDrzN1AvD7vSGbZLN7dupFHELOhSzgeGOS13ZV\
    leyz8L2fjzX2qANA/gusWCrvn2WON8VcI7vpi8G1ChXMOtHhLtG9TdFl3+pwrSKmUAENxH9/8oD\
    kicb6ZfLRTvXvQfL6OrWcLQDdcfp6XVaMoiyMKZ0P5JcQg7L8Rg6TN21I1BwMmeYG1xpgcjtq20\
    FYCjuiGINvvF+hm7kpaCJXHteYKddklwBEnC9MWcTKRvT1GnnDCAVXUqOdBALU7cP4As2FMtJq9\
    a846Ru9ZHulnflaK6KR713uT5nfTjRyiNB6KcpwNsadJLZN1WR+dG8kfcMwI1/UFDJ4+y8TCbVw\
    3rbuX david.blaisonneau@gmail.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCpk6Inc6RbFmiAVOYSUr0VWPrtubrpt1IqLix\
    htuwSJEJUgg7Qz1cCkIW3ntJX56aXh8voBLnGHSKsuDpCCBMdKPFETCQ3TXPvsnRnzoJHnO43MG\
    xgK2UNeBIt5yrAsB7HAP7DGT2NRelzSwPg1jchzb/01j7wjuKUcGLH8nwfo43A9pAxod68nAAy0\
    MtQqWnPdkMJCe+sxmws3OheQmQjP++9p3c98yHvxeG4Tdoq2V1wz7Krhf5zn2d9WPDaFD6sW/jW\
    m8+NqBuzCDLOHduVtD54D3oOKcnxX3lHIFa+jhCxbRVDqtqonQDQPUJTKbSNomuizsQtMBOe3T2\
    MXtVp david.blaisonneau@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDB22+oh+TaQs5XbJtDY3elDWHnTlPCOwqr0kh\
    wYGn/teOr9hPDtqADPjaymoowNDbFIB9CZleZuLFAIryCplb563EwHoaGllRaBom/Alzj3QRIhx\
    FjhWCBzes+ix6NRqKQZ7wPGB7u5QuQCvqlMgLKDNz2UmPoIoYLrhrFyUE5cxWtZ2vmSMTkOpLwO\
    Aw+9UWMww6jzCLNcQjl25zyfSLXVCLLbuyBUza0SvcyxH4glbjTx3BjdCp6ovDDhO2z9tvofvzF\
    j5ceGlohD40ETiROT9C7hKj7Bn2MPDCKPEEjBVjkh9YkpUD9W6uW/t1loGoajLZeJEbN946L3Lt\
    Yi0wz morgan.richomme@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDmQpGN/RX+qEGmBEzg2RKWBTgKz3WCmv1pPeQ\
    3pAWzK1wdYeAr7JMD2YaId+ckj+k8hn6YspYT1mR9SBeSsapn1a09Nosb0Duta1XS6RaytMKJm1\
    tFlLjv5QUwrIN4P0feNBnafZsLKvPAY/x/E9lOtCu/qCVSkzvlOCxDpVS5ZR2NDlOG+8k3QgflU\
    CwrkRfkVitFVuaAmNwHm9s3gsoXH1dxRFAddBPaHtb96ShUBZeguVg6q6bbwFgBC9tD8fggbKDJ\
    ekSTFn20abH5HRdh/VUWl6X0YwO/JhHkrqh+GLA51IiOBAB02xlwi8Abp8/wYL8x+eH0fThAi7V\
    slRav nicolas.edel@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAntwUdcw9hj6jirxyHBOreSFIVc9TEubOcFgtsXJ\
    1nF0roJtFiTE6pzvvw6eU5s8aBWbWD8CebCRqJO2WrDGy69fxpduV4WNpTbKhkx5JbTKI9e/Yyr\
    uU5Ke7tv4BaTlZpROmJonozDQjGrVfiJPTxIS8rN6cMK5cs7Tdt1V7JM7QGBDs4w8TDqGqXWopD\
    sPkCzZrdy1zG5gQNKQm1vI3MAjED/gwtvJ7UH4bnhLK774V2o6fzQfDd82eqdb8uTCZh/NJH/0n\
    83SzKOKOeiVVklWjtkjRMi/V+r40//WRVcoCcPkbW3XrqJAr+z/Fr0o9NuxPfaMk2GTiHPYpbmL\
    0FQ== rene.robert@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqbo9WQwjZp6Op6awsgRaBdR+gJhbmMHOm+2O\
    l7TJlXe2wV5r2vFQhbmd0GHTdz4g8MH+y7oO5637RoAMZlY4ieZdSVGZZ5HvIoMqoS7FVT6u1kv\
    VTVBmlpEgF4gXLsJi3s4bGlXqVnYA42SoKX65DhvHgLIScL1E6uS63MLRP+clFchNASyhz8CgRP\
    dazE7vy4LJJNE8C6hj0MVSOVasJKMZQAmqjSFkuMy0ECNDj5sQmoPYozpvM9xsP/UWiW8MCWuTh\
    33JRBPH/06OIUMVN0dD+xhfVgYY+aueFRjs4oaMm2kfgiTJsQwuAziLrvnusSse+idq9t49YeiA\
    PGpuZ sylvain.desbureaux@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDy4J5MFwpEp0ucKffPyDKGceCynvUqjlsO3r\
    dJRHNA4MDOXygYdMaqhK8FYB9ti2yqAS5AMOR6niio9s06a7mxOGVDaUvlT8L6UUrZAhFYDhWUe\
    amMWAdlG8DWhzfQkYAchbItgxaaSRa8gqZdoyB6hq+JY17K8OKnItr54twhHhsxvAV5lT5C50mG\
    V7aacrWQme/prwDNZsW0aZMlnRSq6KgFidvAL8YaWbSc4yqZvT57SmNRxeOh/MVKhj+F+kKWWDI\
    8nOX/dDFP+QhZeJx0lXHTYNPgzl2S2IAdrbZBcHUpwcnZofToSqlOBsJ4VOiohRngw+abozAGO3\
    9UAvv+3WkT7e0opRRO+OKQVdzkKszNEnTPqKYl9fMTr4HgAV1CAa7PlWYq5IIDMERfPuc0zP5kU\
    qS2SoUyclUI2gDcJpZ6UCt5MAFIbWNlE8KyaEGtMrM4t8z3Zvn8/IdWAREA0RVHpJVgNtKPvvLI\
    NB8O1J4UDBN4fV5vVN+JIHrpDrxz7hKN5pLbl6j0T4SVqQneb0Whbz63P4WF22ddY+bCP7+C9p3\
    pmDibAQkh0aKTyK3J3snvI2mtZ9QfDlN5mL4Rao288SbsVC9pkYUStowmrGCyUf6Txk7F2pP66+\
    pbx8rTON99Hqcv3r8QbKY/k8ORqIT5SCq3fex4p4hRLBk7Q== veronica.quintunarodrigue\
    z@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBlukmibuf0LMrK5NmZkxIziYrOovBzYG8JmQ\
    KONN3KtRZBy9ooL5JBFfmdWMvdcWoLee23R024DwWGF1ooR9SyolXQ+K7lbsSzw0NR/+feRs3of\
    asxj0bXJsWjC48C7eD8v48Y2PMs5uP5Yp/RMOThHVN7w2gbm2OZGG5MIV1ixML/coqo39clm8dC\
    pFhSZmPfmVFliOBW8dFk+DSoKClVy7FH56qEzKoenLJy/+j0B10/5t6T7Sk1QUWDOfIikBomGa2\
    gZAozgGFnuoK9b7FBwlyZiYbbbL4EShiqkEaY938zYcXr1/bqcZWj/F30hK5ddo6f1IDwr57U6l\
    1W5Vd thierry.hardy@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCaK+5td8aCRvDYRET/9jFBF5tCUG/jwfogC1q\
    PXS6f6ou+jLDiNa2e1l/2SbsiU5GdQzT2dfFe50HHiwn15KX5D/0QDs4W8/TAaSXMFWw2ioe1n1\
    A4kXsN2MwPO6HWWhcpWhj+FCGQvU7dP1Divqb4HOpEO2AIkRMH+pH+v/22EsNHBd8xY/JpoC//n\
    0/kzdQ9Fp5pdkoz7VdQaVowu2UsJBrNKVjhNQd31Lw9H+EGNwxCyV4YTTXQRUQkZmnx6kNrtIMQ\
    n4xEw9eFMjHvjjOxO9c2qt++LB0eNfgAgDMHSDkTUfSuF3Tr1YMFy0h+M/nqXgTNme/HAJbdDY+\
    eFRCL natacha.chereau@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+iN2kiREwh5ALuDoHc+Hgxomq0T5JTVnV6/u\
    A23u7g7C15ejdsMsUcJhd0O4UFBmFkqffBfUej4RKvycbmXMakQvYymPSEOquV0l62vWOCcZMdU\
    +F/alPO67klW4SYSwpwMokNg2BvFuHKBE9IyvG53Gc+zXcIeWdxI/ZFnzNfAsWKan4/eoZN/Azt\
    53AL34z1XgufMLwu/rgLTy/pHtSSZHdEzYswR70AXQWwOdBGbzQNPHzFpUueeWj2QciChFB7DQW\
    y62UfXGg6LZSpOkWepN62o4En2lS1FJ3FPWZUlW345c6CYjGis1VZTk/qwCnnHrLrJ6AcODJ4A9\
    bHaiH matthieu.geerebaert@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDWxCTSrtlqBs9iAJMzzKLypqnL9xVqgzorBCS\
    SyxZS8HtsO/qsvgOfvfqyV9RXeiEAEo+8lEI+8vrqggbrkzA5HL78F6p9IQo22iEF4ibLNkDOTB\
    dhnaxk6MXIwCYmLtSZGDvxGzGkGPVZHVgXoBIDbQwOxo0zy52mHmOhyQHhx0hCN8RkujCn/V/KC\
    O1iw1sxJwFnKmosTkwijDZ51KYd3d7asWYiil8cisbuIVp8HgOkRxcruw4qfrCzSbZqbOY2l6uX\
    08ygwmItOXDAV9yQ33C+ogXkEgTBSk0YlfCXNg64c2UJclG1sQUNTu23Hmjd04fm1Z1AfATNJrp\
    5XGSD romain.gimbert@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAhD+JvsglgiKV65uQMPJumVltkuGYSYQhGoS7ZvK\
    9+yGP0xaHSDfoaKSMNR/hqKJcQjnAsYR/PmexA+caOE4lQyK07kNcQ/JeOqRoDWFaxZBBlvG3eH\
    an6FhlYlNx/x9az7gZaxeD57JUrFcX4NfPksnGHN9rPj7e/P5fPDCAOCU6WSLobp6FM7ChOJ7Xm\
    BFtIq33TgHxKP28r+94EmxnjYnsEJfACF4OBeSexLZ9pmH4dto/EM4BlclHUVml/VrvhUFjCsvW\
    5mvM4u9i7JCaf+bPQyps+FOtRPN73cxS5Jxt/OREwuyyDaB1RWayE4GqHxoc0bulW3AaciGKKyC\
    vww== natacha.mach@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDuQ73S+tsV9B5T5aZGYyComeRF2FLGI9ExomW\
    srf9ijUiIe7UP+JvIClyAt8dXz+VI1a4TZ5FT+k7Ik9qaijZgFJoW4gHR/MSPMUJNXFE//tiWYb\
    Gvq2Qh3Ve3Ca9nuXDcJTn+4Qz9QtLFyg4RzCt2AMYlxuJ96XTNYZJkBn+gNq2YZHuuxx1zyRcQB\
    BzvXNtChgsXnyzARQkCDqO7pJpmRKSS2qm2eitkqAhf800VoRXLhzjG78GFS0Y+G+DnNmH9gXPB\
    hC389NmpNNW3Yh6J5LbstfJ5Oga4lfUALxK7Xrfy516G8w3BINE1OvbutZHgFinZtlPX2ZAOspq\
    R0pZ5 nicolas.schlewitz@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMeq70ETzQMB3qUQhqrt5Ul7WOu6QjCnRy1WW\
    5nV+UnK7wlkjgtkQKDIRUbQX7MQv1DEUUYJ0klxtoOMZsEsSVyWfb9i3df0qGsDYr+PstGWz6qe\
    6QVgz6RePJKNoGkQkzrKOUawDPjRPIVIDXOARZ7asSzk2CzRl5V7a++q2z5gb1BKctIFBiz5Oql\
    q7rXC4oSpZm8X0JHdF5Fbmn9xswUlhyj9kwmOKBMP61I4APwaUlgCqNxtpdkpnbLoyIlURfZ0LX\
    eA7+Wbr/YNfYNokm3ia5OvyZBIAJW612hS/oLHyVrSOp/n52NXvg9GcAHN/g1Wbdizwi5EcJmEd\
    F51vx abdelmuhaimen.seaudi@orange.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRslHbwKe4AzMa9dRXnpEDjrbqMvPuGxG2zzZ\
    KbKWsqtzUhgIAMhUL6vw4ePI34bN6uqq8qsQDDu5JYJ3sO1WwpFhX0ZEvWohQK55oe94kfXfnbu\
    sOwRfd8uiWh2s8lfCL7BlbruKGIJcueX+bMLJfUPo3vo5Ae3bBn+J4sG1tLLpf9UxvuXmnZiRyv\
    QJjCy2f6BOeS2AzRhezkC23IS2gylzG3I3MT1acw3i9wAF2yZ2ca++6MdLb6e6Bu30Y9rO7PAj8\
    76myts4GaBhbXmi1+rjb0HbUm3V9wxG7drKv7C0y0nftBS0Q9yCKqld96WQgseYL3CcwEe4qVqn\
    a7DxWQCJROJRi9/DcSSoL73jPcfSWyRqeutWp55S/ajvseAG2yZExbMiGOmW5qsyAGMigjGik59\
    H7gSJR0RitQtGpoqBzsuFaGgYnpfUkAgnjdqqlLT7iRO6OZAXVVSZODbb6LE+2av4B0Yp7SHx8U\
    Rps4CGBS8HyGHQNelnOmra8SnVmXtOhYU87kdOoPaTU7GkuYiy8ctewSoHdgX40tRbB45AMlc+L\
    avf+4hx/5K/u50gei19tUM9aKf6TmdH8LFOPtwUtMd/4nNVYmuwdEFqTVTkVPYdpIF75N5NDF7d\
    uDRyYakTPG/rvj9l0zT5V7sfc3QA0TVNdHu6NLPHHqvkjsQ== k.opasiak@samsung.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDD4j5y8ScCYqeqrpWbwYY+nLFryKoegPocXSA\
    gPRCGVzRFlFk90poH4I216RLqiQ/WfJpCBYId3qpcmG4e3teqPpketMkh+t5GU7mjU5bSy/lJAu\
    l1gNdMA3pQv2nku05KgChNcM7FpWsvMrGoJ7xVyBsop+oy5K+rKO286JiafxIj38sW5tbXmq4dp\
    Z6hlaeBu9Ahi+X76JINcU5pHhslAC8JprZ/Pc23jRAU8kPYrHmWV+xZ3rutSixc58KwbrVRIZRS\
    B9/F170RKXFGRhMKbCUgsO81kIERxNiEA2OJIkSSqebr8L0jv9MYGArLtW53IEihFU7sjgaU19w\
    qVAHb l.bryndza@samsung.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKWAmoyEaYUmGq1JqwWQdqnKDVcP+ugMDLV90\
    pxw4l/GC3ddOPBvGgmRy8cao07HI3WE7e9EqHaK3AtvGddSUW+t8P7WAgOQLBLfJkaydjbTLYG/\
    gkCwQqUFVwIuTt43GKacP3Yi9Owv0jEkxpp9ioJD8Lat/8P3NJE3Wwa8N+fReuedkPHXDXQg6oP\
    71WmM1a0DjjYy7lQq6k9lNcM2RSXbTHpjJ7WyFRpCoqiAyrXzSYVWRn1oFeplAlLMQK2ItI5CQr\
    3x5dHAu8qGRmAlvztfNyltVKBlnKWx909mLkoqAxvlDLLOmjd7NuazOF3LBz4lCYGr1s7mhAIoO\
    WUanH borislav.glozman@amdocs.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCx/xA4zpJOXj03X3H8jNAZynKGOJvOkqnoRwq\
    1gEw46WDnZoElAkTojjCZSqBXYsmK48uLNMsys0FufL4XPjtYEvjrApooQBXsH+JdGKjm9M16pv\
    MSPjcxov0IQ+GuTrBFEbg6ismmLNgAGdXBvJa4q+Ne0yaPEh3WbffbPEShIUj6wiFFI7pdd4je2\
    Dka2kPrFBQUsJe6qUrQ3nbpXpNg3XGnKm3fqNfKWSw5Lc5UfvKxgLxU+9ur46O63LwgAyako2FY\
    J5dC6RkAe91fpqUNcM4JhjuqmeTrqpi8QEjX4t22Zr6W26Ueepd7uuKyiRnXYd27uwjvHCgC41V\
    0BGiB krzysztof.kuzmicki@nokia.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDZ4N20Uhx1jimnvfWxSJvdqwiwoQFuRYbjClH\
    kQqAjfK+4DROcuKTpEY9qjYJH6sfxmWHKAe10hkhoOzX3x3HysRu1ryFPA5EtXZly3oLzmxkq6O\
    6/pUljomSvu4MwER4dlGsTVys3nDqlgtM+kI4vg02zrfRdArnYz2MkuyGnjnkNXr1YXK0tPk+bc\
    TgPlsF+fRKqcw7lTYnM80XJmezJxjlpnAqrDIt6suKtLRHurlF7oieCz1gKfDYNPCj7ENUtQYDT\
    8LvfYyl7RstdqepYm3rjl/E7ookHgGUsceZePo0NfEOBoFLzbaDsQ+K1XTZjwVKkstx2rZmN3oN\
    dWz9D3WZu1nRZXTW6QotHVrMdHjKKjel/IofpDZ6Q4HinecfitFHzRgQswkP9iHJC2ttN4DKZD+\
    NoUVtCF7MoO7GntYJdJqHOfglyp/eIX4bMna5aGCZuzKg5+HAn2jGu3Gpcpc1T1llLPs80kSPzu\
    W76kR5T4Kgcm8hvFmRN2vtX2kKDmpM2oOzq/hqC4mos+IoLEx5vjO9aHAZ0nsP5P2aFI4tygmuv\
    7+Qmy5Qrl8wLcnTGiJZr6hkupbnDmDCiuztlk4Hv4GAtAnz69LRF5saLX6YuoA0qSwHf1kzOUxA\
    P949Sz8ALUJjEyYtSgsQAM62GZTxC45rACRvfBddDWWTwVw== p.wieczorek2@samsung.com

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCfZxrkuq7Mc0DF0q5vhrgp5YNK4DbbsdnIRaZ\
    SHvDAE9o0v/Ri4oCwsaek7XPvlwIOq98MaXMK2XyIraYPTx8VjXS1Dfg0DoLuJJUbSuGGj2fRiD\
    55MUIZyR4LDO2o6cCMr4fWWfP+kTPOaJCT6WjuwYmRkHAehX3N0Csv5Il1h8mP+dFkCPByD3Qjp\
    kLD2m4RNsRA51rDslhf9jEDDe3EALyrq1N9hG3cLAZO3GzTAza4UynVTr5QdzVHuJY0L2wFz2JJ\
    4j0d++IJZZDkNQOVk8FZ99HXH2jE1wlbN3PIwF85drv6t5KypTgY+g3uF1YJVJv58skc1WUZrHK\
    /vk53txjFYQ5N8xeLJihFrkx+0sX5P9TTpF9QAIZlD9PjdKMXfY5wV7FfklCdY2UlxbRpn/aSXx\
    8vXC6vJEiUmaQNm+lENQzsxo/ZsN3W8WAe+B5QE/+CZ9NtIY+0qZPev1QtvQe1J4HZzF81KeuhZ\
    nXyXYLLQSzU156k3i1qFkY8TUmTjR97i/K31kfx+YiZfpbX2MlzYW3FHptUSjvm8DEOP9Cw1Swv\
    hK2pYE/FrtRE1FTIB8Hhmq/9cS9SpT9O4bdCHdY3femXa77+Cv9sqmzdAsrpDXBmVqe2YliwgHg\
    iFBIwoqsu792ZZ3BlPmRYYLpX69/7zni0XGzSj1ndIOynIQ== seshu.kumar.m@huawei.com

    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICI+NGdpOf/LOWjC1iDYpo+8yqbcvQUm4zXB6S\
    fxBLi amine.ezziatti@orange.com"
